# SPDX-License-Identifier: GPL-2.0-only

import catalyst_parser.spec_parser as spec_parser
import portage
import foxcommon as fc
import os
import subprocess


class Spec(spec_parser.SpecParser):
    def __init__(self, filename):
        self.filename = filename
        super().__init__(filename)

    def check_packages(self):  # returns True on success
        fc.info(f"Beginning package check on {self.filename}.")
        self._not_valid = []

        self._loop = "loop"

        if os.getenv("CI_MERGE_REQUEST_DIFF_BASE_SHA") != None or os.getenv("CI_MERGE_REQUEST_TARGET_BRANCH_SHA") != None or os.getenv("CI_COMMIT_SHA") != None: # running in gitlab
            if not os.path.exists("/tmp/dev"): # https://gitlab.com/gitlab-org/gitlab/-/issues/215457
                self._loop = "loop=/tmp/dev/loop8"
                subprocess.run("mkdir -p /tmp/dev".split(" "))
                subprocess.run("mount -t devtmpfs none /tmp/dev".split(" "))

                for i in range(0,9):
                    subprocess.run(f"mknod -m 0660 /tmp/dev/loop{i} b 7 {i}".split(" "))


        subprocess.run(  # mount snapshot spec is using over repos
            f"mount -o {self._loop} /var/tmp/catalyst/snapshots/gentoo-{self.values['snapshot_treeish']}.sqfs /var/db/repos/gentoo".split(
                " "
            )
        )

        self._mounts = ["/var/db/repos/gentoo"]

        for conf_dir in os.listdir(self.values["portage_confdir"]):
            if "package." in conf_dir and os.path.isdir(
                f"{self.values['portage_confdir']}/{conf_dir}"
            ):
                if not os.path.exists(f"/etc/portage/{conf_dir}"):
                    os.mkdir(f"/etc/portage/{conf_dir}")

                subprocess.run(
                    f"mount --bind {self.values['portage_confdir']}/{conf_dir} /etc/portage/{conf_dir}".split(
                        " "
                    )
                )
                self._mounts.append(f"/etc/portage/{conf_dir}")

        self.portage_db = portage.db[portage.root]["porttree"].dbapi

        for package in self.values["stage4/packages"]:
            if len(self.portage_db.xmatch("bestmatch-visible", package)) == 0:
                if len(self.portage_db.xmatch("match-all", package)) == 0:
                    fc.warn(f"{package} is not a valid package. Offending file: {self.filename}")
                    self._not_valid.append(package)
                else:
                    fc.warn(f"{package} is masked.")
                    self._not_valid.append(package)

        for mount in self._mounts:
            subprocess.run(f"umount {mount}".split(" "))

        if len(self._not_valid) > 0:
            return False

        return True


def main():
    pass  # for now do nothing here, might add functionality later


if __name__ == "__main__":
    main()
